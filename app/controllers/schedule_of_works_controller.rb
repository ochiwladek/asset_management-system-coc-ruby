class ScheduleOfWorksController < ProjectsBaseController
  before_action :set_schedule_of_work, only: [:show, :edit, :update, :destroy]

  # GET /schedule_of_works
  # GET /schedule_of_works.json
  def index
    @schedule_of_works = ScheduleOfWork.all
  end

  # GET /schedule_of_works/1
  # GET /schedule_of_works/1.json
  def show
  end

  # GET /schedule_of_works/new
  def new
    @schedule_of_work = ScheduleOfWork.new
  end

  # GET /schedule_of_works/1/edit
  def edit
  end

  # POST /schedule_of_works
  # POST /schedule_of_works.json
  def create
    @schedule_of_work = ScheduleOfWork.new(schedule_of_work_params)

    respond_to do |format|
      if @schedule_of_work.save
        format.html { redirect_to @schedule_of_work, notice: 'Schedule of work was successfully created.' }
        format.json { render :show, status: :created, location: @schedule_of_work }
      else
        format.html { render :new }
        format.json { render json: @schedule_of_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedule_of_works/1
  # PATCH/PUT /schedule_of_works/1.json
  def update
    respond_to do |format|
      if @schedule_of_work.update(schedule_of_work_params)
        format.html { redirect_to @schedule_of_work, notice: 'Schedule of work was successfully updated.' }
        format.json { render :show, status: :ok, location: @schedule_of_work }
      else
        format.html { render :edit }
        format.json { render json: @schedule_of_work.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedule_of_works/1
  # DELETE /schedule_of_works/1.json
  def destroy
    @schedule_of_work.destroy
    respond_to do |format|
      format.html { redirect_to schedule_of_works_url, notice: 'Schedule of work was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule_of_work
      @schedule_of_work = ScheduleOfWork.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_of_work_params
      params.require(:schedule_of_work).permit(:name, :description,
        sections_attributes:[:id,:name,:description,:_destroy,
          schedule_of_works_items_attributes:[:id,:name,
            :price,:unit_id,:premium,:unitMultiplier,:displayUnit,
            :description,:remarks,:_destroy]])
    end
end
