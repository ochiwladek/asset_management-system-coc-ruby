class CostBooksController < ProjectsBaseController
  before_action :set_cost_book, only: [:show, :edit, :update, :destroy]

  # GET /cost_books
  # GET /cost_books.json
  def index
    @cost_books = CostBook.all
  end

  # GET /cost_books/1
  # GET /cost_books/1.json
  def show
  end

  # GET /cost_books/new
  def new
    @cost_book = CostBook.new
  end

  # GET /cost_books/1/edit
  def edit
  end

  def analysis_of_rate
    @item = CostBookScheduleItem.where(:schedule_of_works_item_id=>params[:id]).take
  end

  def update_schedule_item
    @item = CostBookScheduleItem.where(:schedule_of_works_item_id=>params[:id]).take
    @item.created_at = Time.now
    respond_to do |format|
      if @item.update(cost_book_item_params)
        format.html { redirect_to :back, notice: 'Cost book was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { redirect_to :back }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /cost_books
  # POST /cost_books.json
  def create
    @cost_book = CostBook.new(cost_book_params)

    respond_to do |format|
      if @cost_book.save
        format.html { redirect_to @cost_book, notice: 'Cost book was successfully created.' }
        format.json { render :show, status: :created, location: @cost_book }
      else
        format.html { render :new }
        format.json { render json: @cost_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cost_books/1
  # PATCH/PUT /cost_books/1.json
  def update
    respond_to do |format|
      if @cost_book.update(cost_book_params)
        format.html { redirect_to @cost_book, notice: 'Cost book was successfully updated.' }
        format.json { render :show, status: :ok, location: @cost_book }
      else
        format.html { render :edit }
        format.json { render json: @cost_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cost_books/1
  # DELETE /cost_books/1.json
  def destroy
    @cost_book.destroy
    respond_to do |format|
      format.html { redirect_to cost_books_url, 
        notice: 'Cost book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost_book
      @cost_book = CostBook.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_book_params
      params.require(:cost_book).permit(:name, :specification, :remarks,
       :schedule_of_work_id,:schedule_of_works_item_ids=>[])
    end
      def cost_book_item_params
    params.require(:cost_book_schedule_item).permit(cost_book_item_resources_attributes: [:id, 
      :resource_id, :quantity, :_destroy],over_heads_attributes: [:id, 
      :description, :percentage, :_destroy])
  end
end
