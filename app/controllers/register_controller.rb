class RegisterController < WebsiteController
  def index
  	@tenant = Tenant.new
  end
  def create
  	@tenant = Tenant.new(tenant_params)
  	@tenant.plan = Plan.first
  	if @tenant.save
      flash[:id] = @tenant.id
      	redirect_to registration_complete_path(:id => @tenant.id)
    else
      render :action => "index"
    end
  	
  end
  def complete
      @tenant=Tenant.find(params[:id])
  end
  def tenant_params
      params.require(:tenant).permit(:company_name, :address,:phone,:email,:subdomain,:username,:password)
    end
end
