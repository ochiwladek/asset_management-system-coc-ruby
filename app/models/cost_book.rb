class CostBook < ActiveRecord::Base
  belongs_to :schedule_of_work
  has_many :cost_book_schedule_items
  has_many :schedule_of_works_items, :through => :cost_book_schedule_items, 
  :dependent => :destroy
  	accepts_nested_attributes_for :schedule_of_works_items, 
           :reject_if => :all_blank, 
           :allow_destroy => true
end
