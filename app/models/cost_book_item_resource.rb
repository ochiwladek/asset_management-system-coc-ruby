class CostBookItemResource < ActiveRecord::Base
  before_save :calculate_total
  belongs_to :resource 
  belongs_to :cost_book_schedule_item
  def calculate_total
  	self.total = resource.price * quantity
  end

end
