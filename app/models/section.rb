class Section < ActiveRecord::Base
  belongs_to :schedule_of_work
  	has_many :schedule_of_works_items
	accepts_nested_attributes_for :schedule_of_works_items, 
           :reject_if => :all_blank, 
           :allow_destroy => true
end
