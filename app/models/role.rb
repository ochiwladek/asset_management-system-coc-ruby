class Role < ActiveRecord::Base
	 has_many :assignments
  has_many :permissions, 
           :through => :assignments
  accepts_nested_attributes_for :permissions, allow_destroy: true, reject_if: :all_blank
end
