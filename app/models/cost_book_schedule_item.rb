class CostBookScheduleItem < ActiveRecord::Base
	after_save :update_over_heads
	belongs_to :cost_book
	belongs_to :schedule_of_works_item
	has_many :cost_book_item_resources
	has_many :over_heads
	accepts_nested_attributes_for :cost_book_item_resources, :reject_if => 
	:all_blank, :allow_destroy => true
	accepts_nested_attributes_for :over_heads, :reject_if => 
	:all_blank, :allow_destroy => true
	def total_cost_book_item_resources
		self.cost_book_item_resources.sum(:total)
	end
	def total_over_heads
		self.over_heads.sum(:total)
	end

	def update_over_heads
		self.over_heads.each do |over_head|
			total = self.total_cost_book_item_resources * over_head.percentage/100
			over_head.update_column(:total,total)
		end
	end

	def cost_totals
		total_cost_book_item_resources + total_over_heads
	end

end
