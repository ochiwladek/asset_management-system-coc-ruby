class Project < ActiveRecord::Base
	belongs_to :cost_book
	has_many :project_schedule_items
	has_many :cost_book_schedule_items, :through => :project_schedule_items, 
	:dependent => :destroy
	accepts_nested_attributes_for :cost_book_schedule_items, 
	:reject_if => :all_blank, 
	:allow_destroy => true
end
