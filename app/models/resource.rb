class Resource < ActiveRecord::Base
  enum resource_type: [:material, :equipment, :labor,:other]
  belongs_to :unit
end
