class Tenant < ActiveRecord::Base
	after_create :create_tenant
  attr_accessor :email, :password
  belongs_to :plan
  belongs_to :user
  private

  def create_tenant
  	Apartment::Tenant.create(subdomain)
  	Apartment::Tenant.switch!(subdomain)
  	 	user = User.new(:email => email, 
  		:password => password, :password_confirmation => password)
  		user.save
  		update_attribute(:user,@user)
  end
end
