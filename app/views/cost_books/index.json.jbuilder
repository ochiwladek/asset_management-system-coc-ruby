json.array!(@cost_books) do |cost_book|
  json.extract! cost_book, :id, :name, :specification, :remarks, :schedule_of_work_id
  json.url cost_book_url(cost_book, format: :json)
end
