json.array!(@projects) do |project|
  json.extract! project, :id, :name, :description, :cost_book_id
  json.url project_url(project, format: :json)
end
