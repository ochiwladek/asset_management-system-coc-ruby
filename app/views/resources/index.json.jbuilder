json.array!(@resources) do |resource|
  json.extract! resource, :id, :name, :specification, :type, :unit_id, :price, :remarks
  json.url resource_url(resource, format: :json)
end
