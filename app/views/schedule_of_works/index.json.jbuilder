json.array!(@schedule_of_works) do |schedule_of_work|
  json.extract! schedule_of_work, :id, :name, :description
  json.url schedule_of_work_url(schedule_of_work, format: :json)
end
