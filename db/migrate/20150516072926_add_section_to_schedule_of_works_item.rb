class AddSectionToScheduleOfWorksItem < ActiveRecord::Migration
  def change
    add_reference :schedule_of_works_items, :section, index: true, foreign_key: true
  end
end
