class CreateScheduleOfWorksItems < ActiveRecord::Migration
  def change
    create_table :schedule_of_works_items do |t|
      t.string :name
      t.string :description
      t.text :remarks
      t.references :unit, index: true, foreign_key: true
      t.decimal :price
      t.decimal :premium
      t.decimal :quantity
      t.decimal :priceMultiplier
      t.decimal :displayUnit
      t.belongs_to :schedule_of_work, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
