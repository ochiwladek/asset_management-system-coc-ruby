class CreateResources < ActiveRecord::Migration
  def change
    create_table :resources do |t|
      t.string :name
      t.text :specification
      t.integer :type
      t.references :unit, index: true, foreign_key: true
      t.decimal :price
      t.text :remarks

      t.timestamps null: false
    end
  end
end
