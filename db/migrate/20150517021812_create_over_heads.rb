class CreateOverHeads < ActiveRecord::Migration
  def change
    create_table :over_heads do |t|
      t.string :description
      t.belongs_to :cost_book_schedule_item, index: true, foreign_key: true
      t.decimal :percentage
      t.decimal :total

      t.timestamps null: false
    end
  end
end
