class CreateProjectCostBookScheduleItems < ActiveRecord::Migration
  def change
    create_table :project_cost_book_schedule_items do |t|
      t.belongs_to :cost_book_schedule_item, index: false, foreign_key: true
      t.belongs_to :project, index: false, foreign_key: true
      t.timestamps null: false
    end
  end
end
