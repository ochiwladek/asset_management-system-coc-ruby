class ChangeDescriptionToText < ActiveRecord::Migration
  def change
  	change_column :schedule_of_works_items, :description, :text
  end
end
