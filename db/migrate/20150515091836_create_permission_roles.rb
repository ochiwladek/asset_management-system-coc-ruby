class CreatePermissionRoles < ActiveRecord::Migration
  def change
    create_table :permission_roles do |t|
      t.belongs_to :role, index: true, foreign_key: true
      t.belongs_to :permission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
