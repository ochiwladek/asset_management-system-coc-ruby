class CreateProjectScheduleItems < ActiveRecord::Migration
  def change
    create_table :project_schedule_items do |t|
      t.belongs_to :cost_book_schedule_item, index: true, foreign_key: true
      t.belongs_to :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
