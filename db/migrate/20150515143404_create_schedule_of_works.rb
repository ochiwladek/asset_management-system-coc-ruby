class CreateScheduleOfWorks < ActiveRecord::Migration
  def change
    create_table :schedule_of_works do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
