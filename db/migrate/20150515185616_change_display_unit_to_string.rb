class ChangeDisplayUnitToString < ActiveRecord::Migration
  def change
  	 remove_column :schedule_of_works_items, :priceMultiplier
  	 remove_column :schedule_of_works_items, :displayUnit
     add_column :schedule_of_works_items, :unitMultiplier, :string
     add_column :schedule_of_works_items, :displayUnit, :string
  end
end
