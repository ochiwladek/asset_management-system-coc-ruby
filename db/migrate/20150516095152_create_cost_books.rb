class CreateCostBooks < ActiveRecord::Migration
  def change
    create_table :cost_books do |t|
      t.string :name
      t.text :specification
      t.text :remarks
      t.belongs_to :schedule_of_work, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
