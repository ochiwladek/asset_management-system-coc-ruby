class CreateCostBookScheduleItems < ActiveRecord::Migration
  def change
    create_table :cost_book_schedule_items do |t|
      t.belongs_to :cost_book, index: true, foreign_key: true
      t.belongs_to :schedule_of_works_item, index: true, foreign_key: true
      t.decimal :price

      t.timestamps null: false
    end
  end
end
