class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.string :company_name
      t.text :address
      t.string :phone
      t.belongs_to :plan, index: true, foreign_key: true
      t.belongs_to :user, index: true, foreign_key: true
      t.string :subdomain

      t.timestamps null: false
    end
  end
end
