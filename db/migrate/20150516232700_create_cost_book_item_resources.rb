class CreateCostBookItemResources < ActiveRecord::Migration
  def change
    create_table :cost_book_item_resources do |t|
      t.belongs_to :resource, index: true, foreign_key: true
      t.belongs_to :cost_book_schedule_item, index: true, foreign_key: true
      t.decimal :quantity
      t.decimal :total

      t.timestamps null: false
    end
  end
end
