# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150518051722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "assignments", ["permission_id"], name: "index_assignments_on_permission_id", using: :btree
  add_index "assignments", ["role_id"], name: "index_assignments_on_role_id", using: :btree

  create_table "cost_book_item_resources", force: :cascade do |t|
    t.integer  "resource_id"
    t.integer  "cost_book_schedule_item_id"
    t.decimal  "quantity"
    t.decimal  "total"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "cost_book_item_resources", ["cost_book_schedule_item_id"], name: "index_cost_book_item_resources_on_cost_book_schedule_item_id", using: :btree
  add_index "cost_book_item_resources", ["resource_id"], name: "index_cost_book_item_resources_on_resource_id", using: :btree

  create_table "cost_book_schedule_items", force: :cascade do |t|
    t.integer  "cost_book_id"
    t.integer  "schedule_of_works_item_id"
    t.decimal  "price"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "cost_book_schedule_items", ["cost_book_id"], name: "index_cost_book_schedule_items_on_cost_book_id", using: :btree
  add_index "cost_book_schedule_items", ["schedule_of_works_item_id"], name: "index_cost_book_schedule_items_on_schedule_of_works_item_id", using: :btree

  create_table "cost_books", force: :cascade do |t|
    t.string   "name"
    t.text     "specification"
    t.text     "remarks"
    t.integer  "schedule_of_work_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "cost_books", ["schedule_of_work_id"], name: "index_cost_books_on_schedule_of_work_id", using: :btree

  create_table "over_heads", force: :cascade do |t|
    t.string   "description"
    t.integer  "cost_book_schedule_item_id"
    t.decimal  "percentage"
    t.decimal  "total"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "over_heads", ["cost_book_schedule_item_id"], name: "index_over_heads_on_cost_book_schedule_item_id", using: :btree

  create_table "permission_roles", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "permission_roles", ["permission_id"], name: "index_permission_roles_on_permission_id", using: :btree
  add_index "permission_roles", ["role_id"], name: "index_permission_roles_on_role_id", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.string   "subject_class"
    t.string   "action"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.decimal  "price"
    t.string   "description",     limit: 255
    t.integer  "projects_number"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "project_cost_book_schedule_items", force: :cascade do |t|
    t.integer  "cost_book_schedule_item_id"
    t.integer  "project_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "project_schedule_items", force: :cascade do |t|
    t.integer  "cost_book_schedule_item_id"
    t.integer  "project_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "project_schedule_items", ["cost_book_schedule_item_id"], name: "index_project_schedule_items_on_cost_book_schedule_item_id", using: :btree
  add_index "project_schedule_items", ["project_id"], name: "index_project_schedule_items_on_project_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "cost_book_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "projects", ["cost_book_id"], name: "index_projects_on_cost_book_id", using: :btree

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.text     "specification"
    t.integer  "resource_type"
    t.integer  "unit_id"
    t.decimal  "price"
    t.text     "remarks"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "resources", ["unit_id"], name: "index_resources_on_unit_id", using: :btree

  create_table "role_permissions", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "permission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "role_permissions", ["permission_id"], name: "index_role_permissions_on_permission_id", using: :btree
  add_index "role_permissions", ["role_id"], name: "index_role_permissions_on_role_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "schedule_of_works", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "schedule_of_works_items", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "remarks"
    t.integer  "unit_id"
    t.decimal  "price"
    t.decimal  "premium"
    t.decimal  "quantity"
    t.integer  "schedule_of_work_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "unitMultiplier"
    t.string   "displayUnit"
    t.integer  "section_id"
  end

  add_index "schedule_of_works_items", ["schedule_of_work_id"], name: "index_schedule_of_works_items_on_schedule_of_work_id", using: :btree
  add_index "schedule_of_works_items", ["section_id"], name: "index_schedule_of_works_items_on_section_id", using: :btree
  add_index "schedule_of_works_items", ["unit_id"], name: "index_schedule_of_works_items_on_unit_id", using: :btree

  create_table "schedule_of_workses", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "schedule_of_work_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "sections", ["schedule_of_work_id"], name: "index_sections_on_schedule_of_work_id", using: :btree

  create_table "tenants", force: :cascade do |t|
    t.string   "company_name"
    t.text     "address"
    t.string   "phone"
    t.integer  "plan_id"
    t.integer  "user_id"
    t.string   "subdomain"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "tenants", ["plan_id"], name: "index_tenants_on_plan_id", using: :btree
  add_index "tenants", ["user_id"], name: "index_tenants_on_user_id", using: :btree

  create_table "units", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255
    t.integer  "role"
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree

  add_foreign_key "assignments", "permissions"
  add_foreign_key "assignments", "roles"
  add_foreign_key "cost_book_item_resources", "cost_book_schedule_items"
  add_foreign_key "cost_book_item_resources", "resources"
  add_foreign_key "cost_book_schedule_items", "cost_books"
  add_foreign_key "cost_book_schedule_items", "schedule_of_works_items"
  add_foreign_key "cost_books", "schedule_of_works"
  add_foreign_key "over_heads", "cost_book_schedule_items"
  add_foreign_key "permission_roles", "permissions"
  add_foreign_key "permission_roles", "roles"
  add_foreign_key "project_cost_book_schedule_items", "cost_book_schedule_items"
  add_foreign_key "project_cost_book_schedule_items", "projects"
  add_foreign_key "project_schedule_items", "cost_book_schedule_items"
  add_foreign_key "project_schedule_items", "projects"
  add_foreign_key "projects", "cost_books"
  add_foreign_key "resources", "units"
  add_foreign_key "role_permissions", "permissions"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "schedule_of_works_items", "schedule_of_works"
  add_foreign_key "schedule_of_works_items", "sections"
  add_foreign_key "schedule_of_works_items", "units"
  add_foreign_key "sections", "schedule_of_works"
  add_foreign_key "tenants", "plans"
  add_foreign_key "tenants", "users"
  add_foreign_key "users", "roles"
end
