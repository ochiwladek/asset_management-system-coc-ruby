class SubdomainConstraint
	def self.matches?(request)
		subdomains=%{www}
		request.subdomain.present? && !subdomains.include?(request.subdomain)
	end
end
Rails.application.routes.draw do

  resources :projects
  resources :projects
  resources :projects
  constraints SubdomainConstraint do
    get 'project_management',to:'project_management#index'
    root to: 'dashboard#index'
    resources :users
    devise_for :users ,:path => 'accounts'
    resources :cost_books
    get 'cost_books/:cost_book_id/analysis_of_rate/:id', 
    to: 'cost_books#analysis_of_rate',as: 'analysis_of_rate'
    patch 'cost_books/:id/update_schedule_item', 
    to: 'cost_books#update_schedule_item',as: 'update_schedule_item'
    resources :resources
    resources :schedule_of_works
    resources :units
    resources :roles
  end
  get '/', to:'visitors#index'
  get 'register', to:'register#index'
  get 'registration_complete', to:'register#complete'
  post 'register', to:'register#create'
  # root to: 'visitors#index'
end
